import matplotlib.pyplot as plt
import wntr

da = {   90  : 0.0736,
        110  : 0.0900,
        125  : 0.1022,
        140  : 0.1146,
        160  : 0.1308,
        180  : 0.1472,
        200  : 0.1636,
        225  : 0.1840,
        250  : 0.2046,
        280  : 0.2292,
        315  : 0.2578,
        355  : 0.2906,
        400  : 0.3274,
        450  : 0.3682,
        500  : 0.4092,
        560  : 0.4584,
        630  : 0.5156,
     }


def to_cbs(lbs):
    """ convert l/s to m³/s"""
    return lbs/3600.    

def to_ls(cbs):
    """ convert m³/s to l/s"""
    return cbs*3600.    


def plot_results(wn, results, drop_link=[], drop_junction=[], node_size=120):
    
    plt.style.use('fivethirtyeight')
    
    # Ergebnisse darstellen
    _ = wntr.graphics.plot_network(wn, 
                                                    node_labels=True,
                                                    node_size=node_size,
                                                    node_attribute='elevation', 
                                                    node_colorbar_label='Elevation (m)', 
                                                    )
    plt.show()


    fig,axs = plt.subplots(1,3, figsize=(20,8), sharex=True)

    #_ = wntr.graphics.plot_network(wn, node_attribute='elevation', node_colorbar_label='Elevation (m)', node_labels=True, ax=axs[0,0])
    flowrate = to_ls(results.link['flowrate'])
    flowrate.index /= 3600
    
    if len(drop_link)>0:
        flowrate = flowrate.drop(columns=drop_link)
    flowrate.plot(ax=axs[0])

    p = results.node['pressure'].div(10)
    p.index /= 3600
    if len(drop_junction)>0:
        p = p.drop(columns=drop_junction)
    p.plot(ax=axs[1])

    velocity = results.link['velocity']
    velocity.index /= 3600
    if len(drop_link)>0:
        velocity = velocity.drop(columns=drop_link)
    velocity.plot(ax=axs[2])

    axs[0].set_ylabel('Durchfluss in l/s')
    axs[1].set_ylabel('Versorgungsdruck in bar')
    axs[2].set_ylabel('Fließgeschwindigkeit in m/s')
    for ax in axs:
        ax.set_xlabel('Zeit in h')

    plt.show()
